<?php 
	$activepage = "contact";
	$title = "";
	$desc = "";
?>
<?php include 'header.php'; ?>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZCRp9C23t1xqS42COp-GHoDFlTwyqMeA&sensor=false">
</script>
<script>

function initialize()
{
var myContent = "<img src='/assets/logo.png' /> <div id='left'><p id='name'>Eggvena Byggtjänst</p><p>Skovaktaregården 2</p><p>447 92 Vårgårda</p></div>";
var myCenter=new google.maps.LatLng(58.183013,12.932031);
var myPos=new google.maps.LatLng(58.071357,12.851923);
var mapProp = {
  center:myCenter,
  zoom:9,
  scrollwheel: false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myPos,
  });

marker.setMap(map);

var infowindow = new google.maps.InfoWindow({
  content:myContent,
  });

infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<h2>Kontakt</h2>
<div id="googleMap"></div>
<?php include 'footer.php'; ?>