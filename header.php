<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />

		<title>Eggvena Byggtjänst</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="/css/global.css" type="text/css">
		<link rel="stylesheet" href="/css/bjqs.css">
		<link rel="stylesheet" href="/css/normalize.css">
		<link rel="stylesheet" href="/css/pages.css">

		<!-- Others -->
		<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro|Open+Sans:300' rel='stylesheet' type='text/css'> 
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="/favicon.ico">
		
		<!-- Script -->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src="js/bjqs-1.3.min.js"></script>
		<script type="text/javascript">
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject']=r;
				i[r]=i[r]||function() {
					(i[r].q=i[r].q||[]).push(arguments)
				}
				, i[r].l=1*new Date();
				a=s.createElement(o), m=s.getElementsByTagName(o)[0];
				a.async=1;
				a.src=g;
				m.parentNode.insertBefore(a, m)
			}
			)(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-34855982-4', 'eggvenabyggtjanst.se');
			ga('send', 'pageview');
		</script>
	</head>
	<body>
		<div id="main">
			<div>
				<ul id="header">
					<li
						<?php 
							if($activepage == "home"){
								print ("class='active-menu-item menu-item'");
							}else {
								print ("class='menu-item'");
							}
						?>
					>
						<a id="logo-menu-item" href="/">
							<img src="/assets/logo.png" alt="Snickare, carpenter and logo. Eggvena Byggtjänst logga.">
							<div>
								<div id="company-name">Eggvena Byggtjänst</div>
								<div>info@eggvenabyggtjanst.se</div>
								<div>0705281322</div>
							</div>

						</a>
					</li>
					<li
						<?php 
							if($activepage == "project"){
								print ("class='active-menu-item menu-item'");
							}else {
								print ("class='menu-item'");
							}
						?>
					>
						<a href="/projekt.php">
							<span>Projekt</span>
							<img src="/assets/calendar.png" alt="Ikon av telefon eller kontakt som är minimalistisk" />
	
						</a>
					</li>
					<li
						<?php 
							if($activepage == "gallery"){
								print ("class='active-menu-item menu-item'");
							}else {
								print ("class='menu-item'");
							}
						?>

					>
						<a href="/galleri.php">
							<span>Galleri</span>
							<img src="/assets/gallery.png" alt="Ikon av telefon eller kontakt som är minimalistisk" />
						</a>
					</li>
					<li
						<?php 
							if($activepage == "about-us"){
								print ("class='active-menu-item menu-item'");
							}else {
								print ("class='menu-item'");
							}
						?>
					>
						<a href="/om-foretaget.php" id="test">
							<span>Företaget</span>
							<img src="/assets/info.png" alt="Ikon av telefon eller kontakt som är minimalistisk" />
						</a>
						<span class="menu-item-hover">
							<a href="/om-foretaget.php">Om oss</a>
							<a href="/rotavdrag.php">Rotavdrag</a>
							<a href="/miljopolicy.php">Miljöpolicy</a>
						</span>
						<script>
							$('#test').mouseenter( function() {
								$(".menu-item-hover").animate({height: "120px"}, 200);
							});
							$('.menu-item-hover').mouseleave( function() {
								$(".menu-item-hover").animate({height: "0px"}, 200);
							});

						</script>
					</li>
					<li
						<?php 
							if($activepage == "contact"){
								print ("class='active-menu-item menu-item'");
							}else {
								print ("class='menu-item'");
							}
						?>
					>
						<a href="/kontakt.php">
							<span>Kontakt</span>
							<img src="/assets/phone.png" alt="Ikon av telefon eller kontakt som är minimalistisk" />
						</a>
					</li>
				</ul>
			</div>