<?php 
	$activepage = "home";
	$title = "Snickare i Vårgårda trakten";
	$desc = "";
?>
<?php include 'header.php'; ?>
<div id="content">
	<div id="featured">
		<div id="welcome">
			<h1>Snickare från Vårgårda</h1>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam architecto earum 
				reprehenderit dolore iusto labore sed sunt quasi soluta assumenda a quam aspernatur 
				repudiandae. Fugit aliquam odit architecto assumenda tempore.
			</p>
			<p><?php print $activepage ?></p>
		</div>
			<div id="slider-container">
			<div id="main-slider">
				<ul class="bjqs"> <!--Add title attribute for captions-->
					<li><a href="http://www.google.com"><img src="img/banner01.jpg"></a></li>
					<li><img src="img/banner02.jpg"></li>
					<li><img src="img/banner03.jpg"></li>
				</ul>
			</div>
		</div>
      
		<script class="secret-source">
		jQuery(document).ready(function($) {

			$('#main-slider').bjqs({
				animtype      : 'slide',
				height        : 320,
				width         : 620,
				responsive    : true,
				automatic 	  : true,
				animspeed 	  : 4000,
				randomstart   : true

			});

		});
		</script>					
	</div>
	<div id="triple-container">
		<div>
			<h4>Lorem ipsum</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam architecto earum 
				reprehenderit dolore iusto labore sed sunt quasi soluta assumenda a quam aspernatur 
				repudiandae. Fugit aliquam odit architecto assumenda tempore.
			</p>		</div>
		<div>
			<h4>Lorem ipsum</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam architecto earum 
				reprehenderit dolore iusto labore sed sunt quasi soluta assumenda a quam aspernatur 
				repudiandae. Fugit aliquam odit architecto assumenda tempore.
			</p>		</div>
		<div>
			<h4>Lorem ipsum</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam architecto earum 
				reprehenderit dolore iusto labore sed sunt quasi soluta assumenda a quam aspernatur 
				repudiandae. Fugit aliquam odit architecto assumenda tempore.
			</p>		</div>
	</div>
</div>
<?php include 'footer.php'; ?>
